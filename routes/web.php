<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/pertanyaans', 'PertanyaansController@index')->name('pertanyaan.index');
Route::get('/', 'PertanyaansController@index')->withoutMiddleware('auth');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Auth::routes();

Route::get('/pertanyaans/create', 'PertanyaansController@create')->name('pertanyaan.create');
Route::post('/pertanyaans', 'PertanyaansController@store')->name('pertanyaan.store');
Route::get('/pertanyaans/{pertanyaan}', 'PertanyaansController@show')->name('show');
// Route::get('/pertanyaans', 'PertanyaansController@index')->name('pertanyaan.index');
// Tombol Up Down
Route::post('/pertanyaans/updown', 'PoinPertanyaanController@store')->name('pertanyaan.like');
Route::post('/jawaban/updown', 'PoinJawabanController@store')->name('jawaban.like');

// Komentar Pertanyaan
Route::post('/pertanyaans/komentar', 'KomentarsController@store')->name('komentar.store');

// Jawaban
Route::get('/pertanyaans/jawaban/create', 'JawabansController@create')->name('jawaban.create');
Route::post('/pertanyaans/jawaban', 'JawabansController@store')->name('jawaban.store');

// Komentar Jawaban
Route::post('/pertanyaans/jawaban/komentar', 'KomentarJawabanController@store')->name('komentarjawaban.store');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
    
});
