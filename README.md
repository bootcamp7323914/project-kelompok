Saat ini semakin banyak masyarakat Indonesia di usia produktif mulai belajar tentang programming. Media pembelajaran sangat mudah ditemui dengan hanya mengandalkan kata kunci di mesin pencarian. Namun untuk para pembelajar pemula terutama bagi mereka yang kesulitan memahami literatur dalam bahasa Inggris kebanyakan menyerah ketika mendapati error ketika mencoba mempelajari materi lewat praktek langsung. Saat ini mereka menggunakan media komunikasi seperti grup telegram komunitas untuk bertanya tapi sangat sedikit dari anggota grup yang dapat membantu karena masih terlalu sulit untuk bertanya tentang pemrograman melalui aplikasi chatting.

Solusi yang ditawarkan:
Membuat aplikasi untuk diskusi dan tanya jawab seputar pemrograman seperti Stack Overflow dalam Bahasa Indonesia.

{
    "name": "laravel/laravel",
    "type": "project",
    "description": "The Laravel Framework.",
    "keywords": [
        "framework",
        "laravel"
    ],
}

NAMA KELOMPOK 
1.Syukri asmaputra
2.Nanang Rahmat Akbari

LINK ERD
https://drive.google.com/drive/folders/1-TqAa_-SRIBA5d8xcnnGb4IKNDnV6Tfv

Link Video
https://drive.google.com/drive/folders/1-TqAa_-SRIBA5d8xcnnGb4IKNDnV6Tfv
